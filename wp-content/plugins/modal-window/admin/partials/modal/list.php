<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<div class="metabox-holder">
<div class="meta-box-sortables">     
  <table class="wp-list-table widefat fixed ">
    <thead>
      <tr>
	    <th><u><?php esc_attr_e("Order", "umw") ?></u></th>
	  <th><u><?php esc_attr_e("Name", "umw") ?></u></th> 
	  <th><u><?php esc_attr_e("Shortcode", "umw") ?></u></th>
	    <th><u><?php esc_attr_e("ID", "umw") ?></u></th>
        <th></th>
        <th></th>
		<th></th>
      </tr>
    </thead>
    <tbody>
      <?php
           if ($resultat) {
			   $i = 0;
			   foreach ($resultat as $key => $value) {
				   $i++;			   
				   $id = $value->id;
				   $title = $value->title;        
                ?>
      <tr>
	    <td><?php echo "$i"; ?></td>
	    <td><?php echo $title; ?></td>
        <td><?php echo "[modalsimple id=$id]"; ?></td>
		<td><?php echo "$id"; ?></td>         
        <td><u><a href="admin.php?page=wow-modalsimple&wow=add&act=update&id=<?php echo $id; ?>"><?php esc_attr_e("Edit", "umw") ?></a></u></td>
		<td><u><a href="admin.php?page=wow-modalsimple&info=del&did=<?php echo $id; ?>"><?php esc_attr_e("Delete", "umw") ?></a></u></td>
		<td><?php if($count<3){; ?><u><a href="admin.php?page=wow-modalsimple&wow=add&act=duplicate&id=<?php echo $id; ?>"><?php esc_attr_e("Duplicate", "umw") ?></a></u><?php } ?></td>        
      </tr>
      <?php
	  if($i>3) break;
            }
        } else {
            ?>
      <tr>
        <td><?php esc_attr_e("No Record Found!", "umw") ?></td>
      <tr>
        <?php } ?>
    </tbody>
  </table>
</div>
</div>