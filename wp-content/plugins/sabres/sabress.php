<?php
/*
    Plugin Name: Sabres Cloud Agent
    Plugin URI: http://www.sabressecurity.com
    Description: Protect and monitor your Wordpress site using Sabres Security advanced cyber defense platform.

    Version: 0.4.15
    Author: Sabres Security Team
    Author URI: http://www.sabressecurity.com
    License: GPL2
*/

/*
    Copyright 2012  Sabres Security  (email : info@sabressecurity.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


define( 'SBS_VERSION', '0.4.15' );
define( 'SBS_DB_VERSION', '0.2.9' );
if (defined('EXECUTE_DEFINES_ONLY'))
  return;

define( 'SBS_APP_SALT', 'b827v2b9nw893s' );
define( 'SBS_PATH', __DIR__ );
define( 'SABRES_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'SABRES_PLUGIN_BASE_NAME',plugin_basename( __FILE__ ));
define( 'SBR_INC_DIR', SABRES_PLUGIN_DIR . '/_inc/' );
define( 'SBS_PLUGIN_URL', plugins_url(plugin_basename(__DIR__)) );
define( 'SBS_MAIN_PLUGIN_FILE', __FILE__ );

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !function_exists( 'add_action' ) ) {
    exit;
}

require_once ABSPATH . 'wp-admin/includes/plugin.php';

// Shutdown
function SBS_shutdown_function()
{
    $error = error_get_last();

    if ( !empty( $error['type'] ) ) {

        $error_type = $error['type'];
        $error_bad = in_array( $error_type, array(
            E_ERROR,
            E_CORE_ERROR,
            E_COMPILE_ERROR,
            E_USER_ERROR,
            E_RECOVERABLE_ERROR
        ) );

        if ( $error_bad && !empty( $error['file'] ) ) {
            $error_file = $error['file'];
            $error_dir = dirname( $error_file );
            $current_dir = dirname( __FILE__ );

            if ( (stripos($error_dir,dirname(SABRES_PLUGIN_BASE_NAME))!==false) && function_exists( 'deactivate_plugins' ) ) {

                @file_put_contents(SABRES_PLUGIN_DIR.'/activation.log',date("Y-m-d H:i:s").' Plugin is shutting down due to an unexpected error: '.var_export($error,true).PHP_EOL,FILE_APPEND | LOCK_EX);

                deactivate_plugins( SABRES_PLUGIN_BASE_NAME, true );

                if (class_exists('SbrSettings') && class_exists('SBS_Server')) {
                  $settings=SbrSettings::instance();
                  $data = [
                      'websiteServerToken' => $settings->websiteSabresServerToken,
                      'reason' => 'error'
                  ];
                  $data = array_merge( $error, $data );
                  if (class_exists('SbrUtils'))
                      $data['type']=SbrUtils::friendly_error_type($data['type']);
                  $server=SBS_Server::getInstance();
                  $server->call( 'plugin-deactivated', '', $data);
                }

            }
        }
    }
}


if ( !defined( 'SBS_RPC' ) ) {
  register_shutdown_function( function () {
      SBS_shutdown_function();
  } );
}





require_once SBR_INC_DIR . 'sbr_utils.php';
require_once SBR_INC_DIR . 'settings.php';
require_once SABRES_PLUGIN_DIR . '/library/fail.php';

$downloadVendorsExists=file_exists(SABRES_PLUGIN_DIR.'/downloadVendors');
Sabres::$settings=SbrSettings::instance();
$preInstallModeActive=strcasecmp( SbrSettings::instance()->preInstall, 'true') == 0;

if ($downloadVendorsExists || //indicates this is production intallation
    $preInstallModeActive )  //we are in preInstall phase go to download vendors mode
  {
  if ($downloadVendorsExists && !$preInstallModeActive)
    SbrSettings::instance()->preInstall="True"; //activate the mode


  define('PRE_INSTALL_INC','TRUE');
  require_once SABRES_PLUGIN_DIR . '/admin/pre_install.php';
  return;
}
require_once '_inc/check-sabres-config.php';

require_once SABRES_PLUGIN_DIR . '/library/include.php';

require_once SBR_INC_DIR . 'class.singleton.php';
require_once SBR_INC_DIR . 'class.event.manager.php';
require_once SBR_INC_DIR . 'sbr_tfa.php';
require_once SBR_INC_DIR . 'black_list.php';
require_once SBR_INC_DIR . 'class.logger.php';
require_once SBR_INC_DIR . 'server.php';
require_once SBR_INC_DIR . 'class.tfa.php';



class Sabres
{

    const CLIENT_API_URL = 'https://ca-gateway.sabressecurity.com';
    const CLIENT_API_URL_PLAIN = 'http://ca-gateway.sabressecurity.com';
    // const UPDATE_URL = 'http://wp-plugin.sabressecurity.com/download';


    private $SuspiciousRequest = array();
    private $MySqlQueries = array();

    public static $settings;

    public static $server;
    public static $logger;
    /**
     * @var SBS_EventManager
     */
    public static $eventManager;

    public static $modules = [ 'Lifecycle' ];

    public static $firewall;

    public static $aes_key = '';
    public static $aes_iv = '';

    public static $unique_id;
    public static $unique_id_hash;
    public static $request_unique_id;
    public static $request_data = array();
    public static $request_file_name;

    private static $response_ob_content;

    public static function init()
    {
        try {
            // Reload
            if ( strcasecmp( self::$settings->reload, 'true' ) == 0 ) {
                self::$settings->reload = '';
            }

            self::check_version();

            // CRON
            $sabres_hourly_cron_schedule = wp_next_scheduled( 'sabres_hourly_cron' );
            if ( !$sabres_hourly_cron_schedule || self::$settings->trigger_hourly_cron_now=='true') {
                wp_clear_scheduled_hook( 'sabres_hourly_cron' );
                wp_schedule_event( time(), 'hourly', 'sabres_hourly_cron' );
                if (self::$settings->trigger_hourly_cron_now=='true')
                  self::$settings->trigger_hourly_cron_now='false';
                self::hourly_cron();
            }
            add_action( 'sabres_hourly_cron', 'Sabres::hourly_cron' );

            $sabres_daily_cron_schedule = wp_next_scheduled( 'sabres_daily_cron' );
            if ( !$sabres_daily_cron_schedule ) {
                wp_clear_scheduled_hook( 'sabres_daily_cron' );
                wp_schedule_event( time(), 'daily', 'sabres_daily_cron' );

                self::daily_cron();
            }
            add_action( 'sabres_daily_cron', 'Sabres::daily_cron' );

            // Active
            if ( !strcasecmp( self::$settings->isActive, 'true' ) == 0 ) return;

            // Server offline
            if ( strcasecmp( self::$settings->server_offline, 'true' ) == 0 ) return;

            // Check tokens
            if ( self::$settings->websiteSabresServerToken === '' || self::$settings->websiteSabresClientToken === '' ) return;

            // Request data
            self::init_request_data();

            // Current user
            $current_user = wp_get_current_user();
            if ( !empty( $current_user ) ) {
                if ( isset( $current_user->data->user_login ) ) {
                    self::$request_data['username'] = $current_user->data->user_login;
                }
            }

            // Cookies
            self::send_cookies();

            // Register hooks
            add_action( 'template_redirect', array( 'Sabres', 'hook_template_redirect' ) );
            add_action( 'wp_login', array( 'Sabres', 'hook_wp_login' ), 10, 2 );
            add_action( 'login_form_tfa', array( 'Sabres', 'login_form_tfa' ) );
            add_action( 'wp_head', array( 'Sabres', 'hook_wp_head' ) );
            add_action( 'wp_footer', array( 'Sabres', 'hook_wp_footer' ) );
            add_action( 'login_footer', array( 'Sabres', 'hook_login_footer' ) );
            add_action( 'shutdown', array( 'Sabres', 'hook_shutdown' ) );
            add_action( 'granted_super_admin', array( 'Sabres', 'privilege_granted' ) );
            add_action( 'set_user_role', array( 'Sabres', 'set_user_role' ), 10, 3 );
            add_action( 'add_user_role', array( 'Sabres', 'add_user_role' ), 10, 2 );
            add_action( 'wp_login_failed', array( 'Sabres', 'login_failed' ), 10, 1 );
            add_action( 'user_register', array( 'Sabres', 'user_register' ), 10, 1 );

            add_filter( 'status_header', array( 'Sabres', 'hook_status_header' ) );

            if ( is_admin() ) {
                add_action( 'admin_footer', array( 'Sabres', 'hook_admin_footer' ) );
            }
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Init', $error_message );
        }
    }

    public static function init_variables()
    {

        self::$request_file_name = SBS_Net::get_current_filename();

        if ( strlen( self::$settings->symmetricEncryptionKey ) == 40 ) {
            self::$aes_key = substr( self::$settings->symmetricEncryptionKey, 0, 32 );
            self::$aes_iv = substr( self::$settings->symmetricEncryptionKey, 24, 16 );
        }

        if ( isset( $_COOKIE ) && !empty( $_COOKIE['sbs_uid'] ) ) {
            self::$unique_id = trim( $_COOKIE['sbs_uid'] );
        }

        if ( empty( self::$unique_id ) ) {
            self::$unique_id = hash_hmac( 'sha1', time(), SBS_APP_SALT );
        }

        if ( isset( $_COOKIE ) && empty( $_COOKIE['sbs_huid'] ) ) {
            self::$unique_id_hash = sha1( self::$unique_id . self::$settings->websiteSabresServerToken );
        }

        self::$request_unique_id = SBS_Crypto::get_random_hash();
    }

    public static function init_services()
    {
        self::$logger = SBS_Logger::getInstance();
        self::$server = SBS_Server::getInstance();
        self::$eventManager = SBS_EventManager::getInstance();

    }

    public static function init_modules()
    {
        foreach(self::$modules as $module) {
            $filename = SBR_INC_DIR . '/modules/class.' . strtolower( $module ) . '.php';
            if (file_exists($filename)) {
                require_once $filename;

                $className = 'SBS_' . $module;

                if ( class_exists( $className ) ) {
                    /** @var SBS_Module $module */
                    $module = $className::getInstance();
                    $module->init(self::$eventManager);
                }
            }
        }
    }

    public static function privilege_granted($user_id)
    {
        self::$eventManager->event_trigger('privilege.grant', [$user_id] );
    }

    public static function set_user_role($user_id, $role, $old_roles)
    {
        self::$eventManager->event_trigger('set.user.role', [$user_id, $role, $old_roles] );
    }

    public static function login_failed($username)
    {
        self::$eventManager->event_trigger('login.failed', [$username]);
    }

    public static function user_register($user_id)
    {
        self::$eventManager->event_trigger('user.register', [$user_id]);
    }

    public static function add_user_role($user_id, $role) {
        self::$eventManager->event_trigger('add.user.role', [$user_id, $role]);
    }

    public static function init_request_data()
    {
        self::$request_data = array_merge( self::$request_data, array(
            'uniqueid' => self::$unique_id,
            'req_num' => self::$request_unique_id,
            'real_addr_calc' => SBS_Net::get_real_ip_address(),
            'ServerTime' => SBS_Datetime::get_microtime_string(),
            'websiteSabresServerToken' => self::$settings->websiteSabresServerToken,
            'action' => 'trackServerRequest'
        ) );
    }

    private static function check_version()
    {
        $installed_version = get_option( 'sbs_version_number' );

        if ( empty( $installed_version ) || $installed_version != SBS_DB_VERSION ) {
            self::update_db();

            update_option( 'sbs_version_number', SBS_DB_VERSION );
        }
    }

    public static function firewall_settings( $topic, $data, $purge = null )
    {
        if ( !empty( $topic ) && !empty( $data ) ) {
            $firewall = SBS_Firewall::getInstance();

            switch ( $topic ) {
                case 'countries':
                    $firewall->add_countries( json_decode( $data, JSON_OBJECT_AS_ARRAY ) );
                    break;
                case 'cookies':
                    $firewall->add_unique_ids( json_decode( $data, JSON_OBJECT_AS_ARRAY ) );
                    break;
                case 'custom':
                    $firewall->add_custom_range( json_decode( $data, JSON_OBJECT_AS_ARRAY ), $purge );
                    break;
            }
        }
    }

    public static function get_client_api_url()
    {
        if ( self::$settings->https == '' || strcasecmp( self::$settings->https, 'true' ) == 0 ) {
            return self::CLIENT_API_URL;
        } else {
            return self::CLIENT_API_URL_PLAIN;
        }
    }

    public static function update_plugin( $update_file_name )
    {
        if ( empty( $update_file_name ) ) {
            SBS_Error::throwError( 'Update filename cannot remain empty' );
        }

        //self::UPDATE_URL
        $update_plugin_url = SbrUtils::t( 'plugin_download_url' ) . '/' . $update_file_name;
        $update_full_path = SABRES_PLUGIN_DIR . '/temp/update/update.zip';
        $update_file_path = dirname( $update_full_path );
        $update_extract_path = $update_file_path . '/extract';

        SBS_IO::delete_folder( $update_file_path, SABRES_PLUGIN_DIR );

        if ( !is_dir( $update_file_path ) ) {
            @mkdir( $update_file_path, 0755, true );
        }

        if ( !is_dir( $update_file_path ) ) {
            SBS_Error::throwError( 'Update folder cannot be created due to insufficent permissions' );
        }

        SBS_Net::download_file( $update_plugin_url, $update_full_path, 'application/zip' );

        if ( !is_dir( $update_extract_path ) ) {
            @mkdir( $update_extract_path, 0755, true );
        }

        if ( !is_dir( $update_extract_path ) ) {
            SBS_Error::throwError( 'Update extract folder cannot be created due to insufficent permissions' );
        }

        SBS_Zip::extract_file( $update_full_path, $update_extract_path );

        SBS_IO::copy_folder( $update_extract_path, SABRES_PLUGIN_DIR . '/../' );

        SBS_IO::delete_folder( $update_file_path, SABRES_PLUGIN_DIR );
    }

    private static function write_client_script()
    {
        $url = preg_replace( '#^https?:#', '', SBS_PLUGIN_URL );

        $unique_id = self::$unique_id;
        $request_unique_id = self::$request_unique_id;
        $client_token = self::$settings->websiteSabresClientToken;
        $api_url = self::get_client_api_url() . '/client-agent-gateway';

        echo <<<EOL
<script type="text/javascript">
    "use strict";

    if (typeof _sbs === 'undefined') {
        var _sbs = [];
        _sbs.push(['u', '$unique_id']);
        _sbs.push(['r', '$request_unique_id']);
        _sbs.push(['t', '$client_token']);
        _sbs.push(['cg', '$api_url']);

        (function(u) {
            "use strict";

            _sbs.push(['pu', u]);

            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = u + '/scripts/sbs.js?r=' + Math.random();

            var l = document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] || document.getElementsByTagName('html')[0];

            if (l) l.appendChild(s);
        })('$url');
    }
</script>
EOL;
    }

    public static function dispatch_request( $custom = null )
    {
        $prefixes = array(
            'request' => 'req_',
            'response' => 'res_',
            'server' => 'srv_'
        );
        $requestHeadersNames = array( 'Authorization', 'Cookie', 'Request_Method', 'Content-Length', 'Forwarded', 'From', 'Host', 'Origin', 'Proxy-Authorization', 'Referer', 'User-Agent', 'Via', 'X-Requested-With', 'DNT', 'X-Forwarded-For', 'X-Forwarded-Host', 'X-Forwarded-Proto', 'Front-End-Https', 'X-Http-Method-Override', 'X-Csrf-Token', 'X-CSRFToken', 'X-XSRF-TOKEN' );
        $responseHeadersNames = array( 'Access-Control-Allow-Origin', 'Cache-Control', 'Link', 'Location', 'Pragma', 'Refresh' );
        $serverVariablesNames = array( 'Server_Addr', 'Server_Name', 'Request_Method', 'Https', 'Remote_Addr', 'Remote_Host', 'Remote_User', 'Redirect_Remote_User', 'Server_Admin', 'Request_Uri', 'Php_Auth_User', 'Php_Auth_Pw', 'Auth_Type' );
        $excludedServerVariablesNames = array( 'argv', 'argc', 'Server_Software', 'Server_Admin', 'Path', 'SystemRoot', 'Comspec', 'pathext', 'Document_Root', 'Server_Signature', 'Script_Filename' );

        $requestHeaders = array();
        $responseHeaders = array();
        $serverVariables = array();

        foreach ( $_SERVER as $key => $value ) {
            if ( strpos( strtolower( $key ), 'http_' ) === 0 ) {
                $newKey = str_replace( '_', '-', substr( $key, 5 ) );
                $requestHeaders[trim( $newKey )] = !is_array( $value ) ? trim( $value ) : $value;
            } else {
                $serverVariables[trim( $key )] = !is_array( $value ) ? trim( $value ) : $value;
            }
        }
        foreach ( headers_list() as $header ) {
            list( $key, $value ) = explode( ':', $header, 2 );

            $responseHeaders[trim( $key )] = trim( $value );
        }

        self::$request_data = array_merge( self::$request_data,
            SBS_Array::array_refactor_keys( SBS_Array::array_intersect_assoc_key( $requestHeaders, $requestHeadersNames ), $prefixes['request'] ),
            SBS_Array::array_refactor_keys( SBS_Array::array_intersect_assoc_key( $responseHeaders, $responseHeadersNames ), $prefixes['response'] ),
            SBS_Array::array_refactor_keys( SBS_Array::array_diff_assoc_key( SBS_Array::array_intersect_assoc_key( $serverVariables, $serverVariablesNames ), $excludedServerVariablesNames ), $prefixes['server'] )
        );

        $payload = array_merge(
            SBS_Array::array_refactor_keys( SBS_Array::array_diff_assoc_key( $requestHeaders, $requestHeadersNames ), $prefixes['request'] ),
            SBS_Array::array_refactor_keys( SBS_Array::array_diff_assoc_key( $responseHeaders, $responseHeadersNames ), $prefixes['response'] ),
            SBS_Array::array_refactor_keys( SBS_Array::array_diff_assoc_key( $serverVariables, array_merge( $serverVariablesNames, $excludedServerVariablesNames ) ), $prefixes['server'] )
        );

        ksort( self::$request_data );
        self::$request_data = array_change_key_case( self::$request_data, CASE_LOWER );

        ksort( $payload );
        self::$request_data['payload'] = array_change_key_case( $payload, CASE_LOWER );

        if ( !empty( self::$response_ob_content ) ) {
            self::$request_data['res_content-length'] = strlen( self::$response_ob_content );
        }

        if ( !empty( self::$SuspiciousRequest ) &&
            ( !empty( self::$SuspiciousRequest['POST'] ) ||
                !empty( self::$SuspiciousRequest['GET'] ) )
        ) {
            self::$request_data['SuspiciousRequest'] = json_encode( self::$SuspiciousRequest );
            self::$request_data['MySqlQueries'] = json_encode( self::$MySqlQueries );
            SbrUtils::debug_log( "send MySqlQueries: {" . self::$request_data['SuspiciousRequest'] . "}" );
        }

        self::$request_data['payload'] = json_encode( self::$request_data['payload'] );
        self::$request_data = array_change_key_case( self::$request_data, CASE_LOWER );

        if ( !empty( $custom ) ) {
            self::$request_data = array_merge( self::$request_data, $custom );
        }

        $res = self::$server->call( 'server-agent-gateway', null, self::$request_data );

        return $res;
    }

    public static function activate_plugin( $settings )
    {
        if ( !empty( $settings ) ) {
            return self::set_settings( $settings );
        }
    }

    public static function get_settings( $fields = null )
    {
        return self::$settings->get_json( $fields );
    }

    public static function set_settings( $settings = null )
    {
        parse_str( $settings, $insettings );

        if ( empty( $insettings ) )
            self::exit_with_error( 'Can not load settings in' );

        return self::$settings->set_values( $insettings );
    }

    public static function get_files_info()
    {
        require_once( SBR_INC_DIR . 'class.sbrdirit.php' );

        $dir_it = new SbrDirIt();

        //return json_encode($dir_it->recur_dir_it_filtered( ABSPATH, '/^(?:wp-)/', '', self::$settings->maxDepth) );
        //return $dir_it->recur_dir_it( ABSPATH, -1 );
        return $dir_it->get_core_files_list();
    }

    public static function get_files( $path, $recursive = null )
    {
        $ret = array();

        $folders = glob( realpath( $path ) . "/*", GLOB_ONLYDIR );
        $files = glob( realpath( $path ) . "/*", GLOB_BRACE );

        foreach ( $files as $file ) {
            $ret[] = $file;
        }

        if ( $recursive ) {
            foreach ( $folders as $folder ) {
                $folder_files = self::get_files( $folder, $recursive );
                $ret = array_merge( $ret, $folder_files );
            }
        }

        return $ret;
    }

    public static function get_core( $files = null )
    {
        global $wp_version;

        $ret = array(
            'version' => $wp_version,
            'files' => array()
        );

        $get_files = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );

        if ( $get_files ) {
            $files_inv = self::get_files( rtrim( ABSPATH, '/\\' ) );
            $files_inv = array_merge( $files_inv, self::get_files( ABSPATH . 'wp-admin', true ) );
            $files_inv = array_merge( $files_inv, self::get_files( ABSPATH . 'wp-includes', true ) );

            list( $files_inv, $failed_files) = SbrUtils::exclude_no_readable( $files_inv );

            if ( count( $failed_files ) ) {
                $logger = SBS_Logger::getInstance();
                $logger->log( 'warning', "RPC", "Core inventory", implode( ", ", $failed_files ) );
            }

            foreach ( $files_inv as $file ) {
                if ( !is_dir( $file ) ) {
                    $file_name = ltrim( str_replace( rtrim( ABSPATH, '/\\' ), '', $file ), '/\\' );
                    $file_name = str_replace( '\\', '/', $file_name );

                    $ret['files'][] = array(
                        'fullPath' => $file_name,
                        'signature' => @md5_file( $file )
                    );
                }
            }
        }

        return $ret;
    }

    public static function get_plugins( $files = null )
    {
        if ( !function_exists( 'get_plugins' ) ) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        $get_files = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );

        $plugins = get_plugins();
        foreach ( $plugins as $plugin_file => &$plugin_attr ) {
            $plugin_attr['Active'] = is_plugin_active( $plugin_file );

            if ( $get_files ) {
                $files = array();

                $result_files = self::get_files( WP_PLUGIN_DIR.'/' . dirname( $plugin_file ), true );

                list( $result_files, $failed_files) = SbrUtils::exclude_no_readable( $result_files );

                if ( count( $failed_files ) ) {
                    $logger = SBS_Logger::getInstance();
                    $logger->log( 'warning', "RPC", "Plugin inventory", implode( ", ", $failed_files ) );
                }

                foreach ( $result_files as $result_file ) {
                    if ( !is_dir( $result_file ) ) {
                        $files[] = array(
                            'Name' => str_replace( WP_PLUGIN_DIR.'/', '', $result_file ),
                            'Signature' => @md5_file( $result_file )
                        );
                    }
                }

                $plugin_attr['Files'] = $files;
            }
        }

        return $plugins;
    }

    public static function generate_signature( $data )
    {
        $str = '';
        ksort( $data );
        foreach ( $data as $k => $v ) {
            if ( $k != 'sig' ) {
                $str .= $k . $v;
            }
        }

        $str .= self::$settings->verifyHashSalt;

        return SBS_Net::base64url_encode( hash( 'sha256', $str, true ) );
    }

    public static function get_themes( $files = null )
    {
        $getFiles = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );
        $themes = array();
        $themesObjects = wp_get_themes();
        $props = array( 'Name', 'ThemeURI', 'Description', 'Author', 'AuthorURI', 'Version', 'Template', 'Status', 'Tags', 'TextDomain' );
        foreach ( $themesObjects as $themeRoot => $themeObj ) {
            $theme = array();
            foreach ( $props as $property ) {
                $theme[$property] = $themeObj->get( $property );
            }
            if ( $getFiles ) {
                $files = $themeObj->get_files( null, -1 );

                list( $files, $failed_files) = SbrUtils::exclude_no_readable( $files );

                if ( count( $failed_files ) ) {
                    $logger = SBS_Logger::getInstance();
                    $logger->log( 'warning', "RPC", "Themes inventory", implode( ", ", $failed_files ) );
                }

                $theme['Files'] = $files;
            }
            $themes[$themeRoot] = $theme;
        }

        return array( 'current' => get_stylesheet(), 'themes' => $themes );
    }

    public static function get_themes_2( $files = null )
    {
        $ret = array(
            'current' => get_stylesheet(),
            'themes' => array()
        );

        $getFiles = ( isset( $files ) && strcasecmp( trim( $files ), 'true' ) == 0 );
        $themes = wp_get_themes();
        $props = array( 'Name', 'ThemeURI', 'Description', 'Author', 'AuthorURI', 'Version', 'Template', 'Status', 'Tags', 'TextDomain' );

        foreach ( $themes as $theme_root => $theme_object ) {
            $theme = array();

            foreach ( $props as $prop ) {
                $theme[$prop] = $theme_object->get( $prop );
            }
            if ( $getFiles ) {
                $theme['Files'] = array();

                $theme_files = $theme_object->get_files( null, -1 );

                list( $theme_files, $failed_files) = SbrUtils::exclude_no_readable( $theme_files );

                if ( count( $failed_files ) ) {
                    $logger = SBS_Logger::getInstance();
                    $logger->log( 'warning', "RPC", "Themes2 inventory", implode( ", ", $failed_files ) );
                }

                foreach ( $theme_files as $theme_file ) {
                    $file_name = ltrim( str_replace( rtrim( ABSPATH, '/\\' ), '', $theme_file ), '/\\' );
                    $file_name = str_replace( '\\', '/', $file_name );

                    $theme['Files'][] = array(
                        'fullPath' => $file_name,
                        'signature' => @md5_file( $theme_file )
                    );
                }
            }
            $ret['themes'][$theme_root] = $theme;
        }

        return $ret;
    }

    public static function exit_with_error( $message )
    {
        $headerMsg = 'internal server error';
        $code = 500;
        $protocol = isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
        header( "$protocol $code $headerMsg", true, $code );

        return json_encode( array( 'error' => $message ) );
    }

    public static function hourly_cron()
    {
        if ( !self::check_server_offline() ) {
            self::check_server_token();
        }
    }

    public static function daily_cron()
    {
        $timestamp = SBS_DB::get_current_time();

        if ( $timestamp ) {
            $time = strtotime( '-28 days', $timestamp );
            $time = date( 'Y-m-d H:i:s', $time );

            self::$logger->clear_entries( null, $time );
        }
    }

    public static function check_server_offline()
    {
        $server_offline = strcasecmp( self::$settings->server_offline, 'true' ) == 0;
        if ( $server_offline ) {
            Sabres::$server->call( 'heartbeat' );

            $server_offline = strcasecmp( self::$settings->server_offline, 'true' ) == 0;
        }

        return $server_offline;
    }

    public static function check_server_token()
    {
        $settings=SbrSettings::instance();
        $update_plugin_url=strcasecmp($settings->update_plugin_url,'true')==0;
        $response_info=null;
        if ( $settings->should_trigger_activation() ) {
            $response_info=self::$server->call( 'activate-plugin-request', '', array(
                'hostName' => SBS_Net::remove_protocol( get_site_url() ),
                'token' => $settings->token,
                'symmetricEncryptionKey' => $settings->symmetricEncryptionKey,
                'verifyHashSalt' => $settings->verifyHashSalt,
                'rpcMethod'=>'GET',
                'pluginURL'=>SBS_PLUGIN_URL
            ) );
        }
        elseif ($update_plugin_url) {
          $response_info=self::$server->call( 'update-plugin-url', '', array(
              'pluginURL' => SBS_PLUGIN_URL,
              'websiteSabresServerToken'=>$settings->websiteSabresServerToken,
              'token' => $settings->token
          ) );
          if (SBS_Server::is_success($response_info)) {
            $body=json_decode(@$response_info['body'],true);
            if (!empty($body) && !empty($body['updated']) && $body['updated']===true) {
              $settings->update_plugin_url='false';
            }
          }
        }
        if (!empty($response_info)) {
          return $response_info;
        }
    }

    public static function run()
    {
        if ( strcasecmp( self::$settings->click_jacking, 'true' ) == 0 ) {
            self::check_click_jacking();
        }

        if ( strcasecmp( self::$settings->login_errors, 'true' ) == 0 ) {
            self::override_login_errors();
        }

        if ( strcasecmp( self::$settings->author_archive, 'true' ) == 0 ) {
            self::disable_author_archive();
        }

        if ( strcasecmp( self::$settings->auto_update, 'true' ) == 0 ) {
            self::enable_auto_update();
        }

        if ( strcasecmp( self::$settings->debug_mode, 'true' ) == 0 ) {
            self::disable_debug_mode();
        }

        if ( strcasecmp( self::$settings->error_handling, 'true' ) == 0 ) {
            self::disable_error_handling();
        }

        if ( strcasecmp( self::$settings->force_ssl, 'true' ) == 0 ) {
            self::force_ssl();
        }

    }

    public static function hook_author_archive()
    {
        if ( is_author() ) {
            $redirect = true;

            if ( current_user_can( 'editor' ) || current_user_can( 'administrator' ) ) {
                $redirect = false;
            }

            if ( $redirect ) {
                self::dispatch_request();

                wp_redirect( home_url(), 301 );
                exit;
            }
        }
    }

    public static function force_ssl()
    {
        if ( !defined( 'FORCE_SSL_LOGIN' ) ) {
            define( 'FORCE_SSL_LOGIN', true );
        }

        if ( !defined( 'FORCE_SSL_ADMIN' ) ) {
            define( 'FORCE_SSL_ADMIN', true );
        }
    }

    public static function disable_debug_mode()
    {
        if ( !defined( 'WP_DEBUG' ) ) {
            define( 'WP_DEBUG', false );
        }

        if ( !defined( 'WP_DEBUG_LOG' ) ) {
            define( 'WP_DEBUG_LOG', false );
        }

        if ( !defined( 'WP_DEBUG_DISPLAY' ) ) {
            define( 'WP_DEBUG_DISPLAY', false );
        }
    }

    public static function disable_error_handling()
    {
        @error_reporting( 0 );

        @ini_set( 'display_errors', 0 );
        @ini_set( 'log_errors', 0 );
    }

    public static function disable_author_archive()
    {
        add_action( 'template_redirect', array( 'Sabres', 'hook_author_archive' ) );
    }

    public static function enable_auto_update()
    {
        if ( !has_filter( 'auto_update_core', '__return_true' ) ) {
            add_filter( 'auto_update_core', '__return_true' );
        }

        if ( !has_filter( 'auto_update_plugin', '__return_true' ) ) {
            add_filter( 'auto_update_plugin', '__return_true' );
        }

        if ( !has_filter( 'auto_update_theme', '__return_true' ) ) {
            add_filter( 'auto_update_theme', '__return_true' );
        }
    }

    public static function check_click_jacking()
    {
        @header( 'X-Frame-Options: SAMEORIGIN' );
    }

    public static function override_login_errors()
    {
        add_filter( 'login_errors', function ( $error ) {
            global $errors;

            $error_codes = $errors->get_error_codes();

            if ( !empty( $error_codes ) ) {
                $error = '<strong>ERROR</strong>: Invalid username or password, please try again.<br />';

            }

            return $error;
        } );
    }

    public static function terminate( $code, $body )
    {
        if ( !headers_sent() ) {
            foreach ( headers_list() as $header ) {
                @header_remove( $header );
            }
        }

        @header( '', true, $code );

        echo $body;

        if ( $code != null && $code >= 400 ) {
            exit( 1 );
        } else {
            exit();
        }
    }

    private static function send_cookies()
    {
        if ( empty( $_COOKIE['sbs_uid'] ) ) {
            $expires = ( 60 * 60 * 24 ) * ( 30 * 12 ) * 4 + time(); // 4 Years

            setcookie( 'sbs_uid', self::$unique_id, $expires, COOKIEPATH, COOKIE_DOMAIN, false, true );
        }

        if ( empty( $_COOKIE['sbs_huid'] ) ) {
            $expires = ( 60 * 60 * 24 ) * ( 30 * 12 ) * 4 + time(); // 4 Years

            setcookie( 'sbs_huid', self::$unique_id_hash, $expires, COOKIEPATH, COOKIE_DOMAIN, false, false );
        }

        // Cache
        $url = SBS_Net::get_actual_url();

        if ( !empty( $url ) ) {
            setcookie( 'sbs_r_' . self::$request_unique_id, strtolower( SBS_Convert::to_hex( $url ) ), 0, COOKIEPATH, COOKIE_DOMAIN, false, false );
        }
    }

    // Hooks
    public static function hook_wp_login($user_login, $user)
    {
        try {
            self::$request_data['action'] = 'wp_login';

            self::dispatch_request();
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - wp_login', $error_message );
        }

        if(strcasecmp( SbrSettings::instance()->mod_tfa_active, 'true' ) == 0) {
            TFA::wp_login( $user_login, $user );
        }

        self::$eventManager->event_trigger('login.success', [$user_login, $user]);

    }

    public static function pre_install() {
        require_once SABRES_PLUGIN_DIR . '/admin/pre_install_tests.php';

        $user = wp_get_current_user();

        if (!isset($user) || !isset($user->roles) || !in_array('administrator',$user->roles)) {
            header('HTTP/1.0 403 Forbidden');
            wp_die();
        }

        //Used as ajax controller for pre install view
        if (!isset($_GET) || !isset($_GET["op"])) {
            header('HTTP/1.1 500 Internal Server Error');
            wp_die();
        }

        $op=$_GET["op"];
        $tests=new Pre_Install_Tests();
        $result=$tests->dispatch($op);
        echo json_encode($result);

        wp_die();
    }

    public static function login_form_tfa() {
        TFA::login_form_tfa();
    }

    public static function hook_wp_head()
    {
    }

    public static function hook_wp_footer()
    {
        try {
            self::write_client_script();
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - wp_footer', $error_message );
        }
    }

    public static function hook_login_footer()
    {
        try {
            self::write_client_script();
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - login_footer', $error_message );
        }
    }

    public static function hook_admin_footer()
    {
        try {
            self::write_client_script();
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - admin_footer', $error_message );
        }
    }

    public static function hook_status_header( $status_header )
    {
        try {
            if ( !empty( $status_header ) )
                list( $protocol, $response_code ) = explode( ' ', $status_header );

            if ( !empty( $response_code ) )
                self::$request_data['response_code'] = $response_code;
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - status_header', $error_message );
        }
    }

    public static function hook_shutdown()
    {
        try {
            self::dispatch_request();
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - shutdown', $error_message );
        }
    }

    public static function hook_template_redirect()
    {
        try {
            if ( function_exists( 'ob_start' ) ) {
                self::$response_ob_content = '';

                ob_start( function ( $buffer ) {
                    self::$response_ob_content .= $buffer;

                    return $buffer;
                }, 1 );
            }
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - template_redirect', $error_message );
        }
    }

    public static function hook_admin_notices()
    {
        $errors = self::check_compatibility();

        if ( in_array( 'php_version', $errors ) ) {
            $notice = sprintf( 'Sabres Cloud Agent has been deactivated, it requires PHP 5.4 or higher in order to operate. Current version installed is %s', PHP_VERSION );
        }

        if ( !empty( $notice ) ) {
            echo "<div class=\"error\"><p>$notice</p></div>";

            self::deactivate_plugin();
        }
    }

    public static function deactivate_plugin()
    {
        if ( function_exists( 'deactivate_plugins' ) ) {
            deactivate_plugins( plugin_basename( __FILE__ ), true );
        }
    }

    public static function check_compatibility()
    {
        $errors = array();

        // Check for required PHP version
        if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
            array_push( $errors, 'php_version' );
        }

        return $errors;
    }

    public static function get_system_info()
    {
        $info = SBS_System::get_info();

        return $info;
    }

    public static function get_ssl_info()
    {
        $info = SBS_System::get_ssl_info();

        return $info;
    }

    public static function install()
    {
        @file_put_contents(SABRES_PLUGIN_DIR.'/activation.log',date("Y-m-d H:i:s").' Plugin activated by user (install)'.PHP_EOL,FILE_APPEND | LOCK_EX);
        try {

            $errors = self::check_compatibility();

            if ( in_array( 'php_version', $errors ) ) {
                exit( sprintf( 'Sabres Cloud Agent requires PHP 5.4 or higher in order to operate. Current version is %s.', PHP_VERSION ) );
            }

            self::update_db();

            self::hourly_cron();
            self::set_session_task( 'attach_account' );
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - install', $error_message );
        }
    }

    private static function update_db()
    {
        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $charset_collate = $wpdb->get_charset_collate();

        $wpdb->hide_errors();

        $table_name = $wpdb->prefix . 'sbs_log';
        $sql = "CREATE TABLE $table_name (
          ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          log_type varchar(100) NOT NULL,
          logger varchar(100) NOT NULL,
          message longtext NOT NULL,
          log_data longtext,
          created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (ID)
			) $charset_collate;";
        @dbDelta( $sql );

        $table_name = $wpdb->prefix . 'sbs_firewall_cookies';
        $sql = "CREATE TABLE $table_name (
          unique_id varchar(100) NOT NULL,
          do_action varchar(45) NOT NULL,
          description varchar(255) DEFAULT NULL,
          expiry int(11) DEFAULT NULL,
          created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at timestamp NULL DEFAULT NULL,
          PRIMARY KEY (unique_id)
			) $charset_collate;";
        @dbDelta( $sql );

        $table_name = $wpdb->prefix . 'sbs_firewall_countries';
        $wpdb->query( "DROP TABLE $table_name" );
        $sql = "CREATE TABLE $table_name (
          code char(2) NOT NULL DEFAULT '',
          do_action varchar(45) NOT NULL,
          description varchar(255) DEFAULT NULL,
          expiry int(11) DEFAULT NULL,
          created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at timestamp NULL DEFAULT NULL,
          PRIMARY KEY (code)
			) $charset_collate;";
        @dbDelta( $sql );

        $table_name = $wpdb->prefix . 'sbs_firewall_custom';
        $sql = "CREATE TABLE $table_name (
          from_ip bigint(20) unsigned NOT NULL,
          to_ip bigint(20) unsigned NOT NULL,
          do_action varchar(45) NOT NULL,
          description varchar(255) DEFAULT NULL,
          expiry int(11) DEFAULT NULL,
          global_rule bit(1) DEFAULT b'0',
          created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at timestamp NULL DEFAULT NULL,
          PRIMARY KEY (from_ip, to_ip)
			) $charset_collate;";
        @dbDelta( $sql );

        $table_name = $wpdb->prefix . 'sbs_scans';
        $sql = "CREATE TABLE $table_name (
          ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          scan_type varchar(200) NOT NULL,
          status varchar(200) NOT NULL,
          created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at timestamp NULL DEFAULT NULL,
          PRIMARY KEY (ID)
			) $charset_collate;";
        @dbDelta( $sql );

        $table_name = $wpdb->prefix . 'sbs_scan_items';
        $ix_name = 'fk_' . $table_name . '_item_parent_idx';
        $fk_name = 'fk_' . $table_name . '_item_parent';

        $sql = "CREATE TABLE $table_name (
          ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          parent_id bigint(20) unsigned NOT NULL,
          item_type varchar(200) NOT NULL,
          item_code varchar(200) NULL,
          item_desc longtext,
          risk_level tinyint(1) unsigned DEFAULT NULL,
          unique_id varchar(32) NOT NULL,
          created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updated_at timestamp NULL DEFAULT NULL,
          PRIMARY KEY (ID),
          KEY $ix_name (parent_id)) $charset_collate;";
        @dbDelta( $sql );

        flush_rewrite_rules();
    }

    public static function uninstall()
    {
        try {
            @file_put_contents(SABRES_PLUGIN_DIR.'/activation.log',date("Y-m-d H:i:s").' Plugin deactivated by user '.PHP_EOL,FILE_APPEND | LOCK_EX);
            require_once SABRES_PLUGIN_DIR.'/admin/admin.php';
            Sabres_Admin::delete_cron_jobs();
            flush_rewrite_rules();
        } catch ( \Exception $e ) {
            $error_message = $e->getMessage();

            Sabres::$logger->log( 'error', 'Hook - uninstall', $error_message );
        }
    }

    public static function set_session_task( $task = '' )
    {
        if ( session_status() == PHP_SESSION_ACTIVE && $task ) {
            if ( !isset( $_SESSION['sabres_tasks'] ) ) {
                $_SESSION['sabres_tasks'] = array();
            }
            $_SESSION['sabres_tasks'][] = $task;
        }
    }

    public static function get_session_task( $task = '' )
    {
        if ( session_status() == PHP_SESSION_ACTIVE && $task && !empty( $_SESSION['sabres_tasks'] ) ) {
            foreach ( $_SESSION['sabres_tasks'] as $sess_task ) {
                if ( $task == $sess_task ) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function unset_session_task( $task = '' )
    {
        if ( session_status() == PHP_SESSION_ACTIVE && $task && !empty( $_SESSION['sabres_tasks'] ) ) {
            foreach ( $_SESSION['sabres_tasks'] as $i => $sess_task ) {
                if ( $task == $sess_task ) {
                    unset( $_SESSION['sabres_tasks'][$i] );
                    return true;
                }
            }
        }
        return false;
    }
}

try {
    // Sessions
    if ( function_exists( 'session_start' ) && session_id() == '' ) {
        @session_start();
    }

    clearstatcache( true );

    // Vendors
    require __DIR__ . '/library/vendor/autoload.php';

    // Settings
    Sabres::init_variables();
    Sabres::init_services();
    Sabres::init_modules();
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    if ( isset( Sabres::$logger ) ) {
        Sabres::$logger->log( 'error', 'Start', $error_message );
    }
}

// Modules
/*try {
    if ( in_array( Sabres::$request_file_name, array( 'wp-login.php', 'wp-register.php' ) ) &&
        strcasecmp( SbrSettings::instance()->mod_tfa_active, 'true' ) == 0
    ) {
        require_once SABRES_PLUGIN_DIR . '/modules/tfa.php';
    }
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    if ( isset( Sabres::$logger ) ) {
        Sabres::$logger->log( 'error', 'TFA', $error_message );
    }
    if (stripos($error_message,'wp_authenticate')>=0) {
      Sabres::$settings->mod_tfa_active='false';
    }
}*/

try {
    if ( !defined( 'SBS_RPC' ) && strcasecmp( SbrSettings::instance()->mod_firewall_active, 'true' ) == 0 ) {
        require_once SABRES_PLUGIN_DIR . '/modules/firewall.php';
    }
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    if ( isset( Sabres::$logger ) ) {
        Sabres::$logger->log( 'error', 'Firewall', $error_message );
    }
}

try {
    require_once SABRES_PLUGIN_DIR . '/modules/scanner.php';
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    if ( isset( Sabres::$logger ) ) {
        Sabres::$logger->log( 'error', 'Scanners', $error_message );
    }
}

try {
    // Run
    if ( !defined( 'SBS_RPC' ) ) {
        add_action( 'init', array( 'Sabres', 'init' ), 1 );

        register_activation_hook( __FILE__, array( 'Sabres', 'install' ) );
        register_deactivation_hook( __FILE__, array( 'Sabres', 'uninstall' ) );

        if ( is_admin() ) {
            require_once SABRES_PLUGIN_DIR . '/admin/admin.php';

            add_action( 'init', array( 'Sabres_Admin', 'init' ), 1 );
            add_action( 'admin_notices', array( 'Sabres', 'hook_admin_notices' ) );
        }
    }
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    if ( isset( Sabres::$logger ) ) {
        Sabres::$logger->log( 'error', 'Init hooks', $error_message );
    }
}

try {
    Sabres::run();
} catch ( \Exception $e ) {
    $error_message = $e->getMessage();

    if ( isset( Sabres::$logger ) ) {
        Sabres::$logger->log( 'error', 'Run', $error_message );
    }
}
