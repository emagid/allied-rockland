<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
define('EXECUTE_DEFINES_ONLY','TRUE');
require_once SABRES_PLUGIN_DIR . '/sabress.php';

class Get_Version {
    public function execute($rpc_data) {
      $res = json_encode( array(
          'version' => SBS_VERSION,
          'dbVersion' => SBS_DB_VERSION
      ) );
      echo $res;
    }

}
