<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/library/net.php';
require_once SABRES_PLUGIN_DIR.'/_inc/settings.php';
require_once SABRES_PLUGIN_DIR.'/_inc/server.php';

class Force_Activate_Plugin {
    public function execute($rpc_data) {
      $res=json_encode(self::activate_plugin_if_necessary(true));
      echo $res;
    }

    Public static function activate_plugin_if_necessary($echo_results) {
      $settings=SbrSettings::instance();      
      if ( $settings->websiteSabresServerToken == '' || $settings->websiteSabresClientToken == '' ) {
            $server = SBS_Server::getInstance();
            return $server->call( 'activate-plugin-request', '', array(
                'hostName' => SBS_Net::remove_protocol( get_site_url() ),
                'token' => $settings->token,
                'symmetricEncryptionKey' => $settings->symmetricEncryptionKey,
                'verifyHashSalt' => $settings->verifyHashSalt,
                'rpcMethod' =>'GET'
            ) );
        }
        else {
          if ($echo_results)
            echo "Plugin is already activated. resetActivation=true can be used to reset";
        }
    }

}
