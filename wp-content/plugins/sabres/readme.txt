=== Sabres Cloud Agent ===
Contributors: sabres
Tags: security, spam, two factor, integrity, firewall, waf
Requires at least: 4.4
Tested up to: 4.6.1
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Protect and monitor your Wordpress site using Sabres Security advanced cyber defense platform.

== Installation ==
1. Upload the plugin zip file to the `/wp-content/plugins` directory and extract the file, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress.
3. Click on Sabres from the sidebar menu and follow the steps (register to the portal and add your website to the portal).
4. Use our advanced portal to fix and learn about the website security issues.
